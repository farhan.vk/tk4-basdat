from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.db import connection
from collections import namedtuple


# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def BuatAppointment (request, feedback=None):

    cursor = connection.cursor()
    x = "SELECT * FROM SIRUCO.JADWAL_DOKTER"
    cursor.execute(x)
    Jadwal_dokter = namedtuplefetchall(cursor)

    context = {
        'Jadwal_dokter':Jadwal_dokter
    }
    return render(request, "appointment/AppointementCreate.html", context)
    
def readappointment (request, feedback=None):
    
    cursor = connection.cursor()
    role = ""
    if (request.session['role'] == 'pengguna_publik'):
        role = 'pengguna_publik'
        x ="SELECT nik from siruco.pengguna_publik where username='"+request.session['username']+"'"
        cursor.execute(x)
        result=namedtuplefetchall(cursor)
        nik_pasien=result[0].nik
        sql = "SELECT * FROM SIRUCO.MEMERIKSA where nik_pasien='"+nik_pasien+"'"
    elif (request.session['role'] == 'dokter'):
        role = 'dokter'
        y = "SELECT * FROM SIRUCO.MEMERIKSA where username_dokter='"+request.session['username']+"'"
    else:
        role ="admin_satgas"
        y = "SELECT * FROM SIRUCO.MEMERIKSA"
    cursor.execute(y)
    cek = namedtuplefetchall(cursor)

    context = {
        'cek':cek,
        'role': role
    }
    return render(request, "BacaAppointment.html", context)

def deleteappointment(request, nik_pasien,nostr,username_dokter,kode_faskes,praktek_shift,praktek_tgl):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("DELETE FROM MEMERIKSA WHERE nik_pasien = %s AND nostr=%s AND username_dokter=%s AND kode_faskes=%s AND praktek_shift=%s AND praktek_tgl=%s",
        [nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl])
    cursor.close()
    message = "Appointment berhasil terhapus"
    response = {'message' : message}
    return redirect('appointment:readappointment')



