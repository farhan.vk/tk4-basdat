from django.urls import path
from .views import *

app_name = 'appointment'
urlpatterns = [
	path('read_appointment', readappointment, name='read_appointment'),
    path('read_appointment/<str:nik_pasien>/<str:nostr>/<str:username_dokter>/<str:kode_faskes>/<str:praktek_shift>/<str:praktek_tgl>', deleteappointment, name='delete_appointment'),
]