from django import forms
from django.db import connection


class FormUpdatePasien(forms.Form):
    pendaftar = forms.CharField(label='Pendaftar', disabled=True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    nik = forms.CharField(label='NIK', disabled=True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    nama = forms.CharField(label='Nama', disabled=True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    no_telp = forms.CharField(label='Nomor Telepon', max_length=20, required=True)
    no_hp = forms.CharField(label='Nomor HP', max_length=12, required=True)

    jalan_ktp = forms.CharField(label='Jalan', max_length=30, required=True)
    kelurahan_ktp = forms.CharField(label='Kelurahan', max_length=30, required=True)
    kecamatan_ktp = forms.CharField(label='Kecamatan', max_length=30, required=True)
    kabkot_ktp = forms.CharField(label='Kabupaten/Kota', max_length=30, required=True)
    provinsi_ktp = forms.CharField(label='Provinsi', max_length=30, required=True)

    jalan_dom = forms.CharField(label='Jalan', max_length=30, required=True)
    kelurahan_dom = forms.CharField(label='Kelurahan', max_length=30, required=True)
    kecamatan_dom = forms.CharField(label='Kecamatan', max_length=30, required=True)
    kabkot_dom = forms.CharField(label='Kabupaten/Kota', max_length=30, required=True)
    provinsi_dom = forms.CharField(label='Provinsi', max_length=30, required=True)

class FormCreateAppointment(forms.Form):
    def get_nik():
        cursor= connection.cursor()
        cursor.execute("select nik,nik from siruco.pasien;")
        kode_response = cursor.fetchall()
        return kode_response

    NikPasien = forms.ChoiceField(label='NIK Pasien', required=True, choices= get_nik())

    EmailDokter = forms.CharField(label = 'Email Dokter', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    KodeFaskes = forms.CharField(label = 'Kode Faskes', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    TanggalPraktek = forms.CharField(label = 'Tanggal Praktek', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    ShiftPraktek = forms.CharField(label = 'Shift Praktek', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    
# class UpdateAppointmentForm(forms.Form):
#     NikPasien=forms.CharField(label = 'NikPasien', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
#     EmailDokter = forms.CharField(label = 'Email Dokter', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
#     ShiftPraktek = forms.CharField(label = 'Shift Praktek', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
#     TanggalPraktek = forms.CharField(label = 'Tanggal Praktek', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
#     KodeFaskes = forms.CharField(label = 'Kode Faskes', disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
#     Rekomendasi = forms.DateField(label=("Rekomendasi"), required=False, widget=forms.TextInput(attrs={'id': 'rekomendasi'}))
