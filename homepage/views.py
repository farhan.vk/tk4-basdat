from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def index(request):
    return render(request, 'index.html')

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def index(request):
    if 'username' in request.session:
        return HttpResponseRedirect(reverse('dashboard:dashboard'))
    response = {}
    return render(request,'index.html',response)

def register(request):
    response = {}
    return render(request,'register.html',response)
