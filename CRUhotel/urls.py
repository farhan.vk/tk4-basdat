from django.urls import path
from .views import *

app_name = 'CRUhotel'
urlpatterns = [
	path('create_hotel', createhotel, name='createhotel'),
    path('list_hotel', listhotel, name='listhotel'),
    path('update_hotel/<str:kode_hotel>', updatehotel, name='updatehotel'),
    
]