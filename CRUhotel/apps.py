from django.apps import AppConfig


class CruhotelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CRUhotel'
