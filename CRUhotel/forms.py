from django import forms
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

class FormCreateHotel(forms.Form):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("""SELECT * FROM HOTEL ORDER BY kode ASC""")
    results = namedtuplefetchall(cursor)
    cursor.close()
    connection.close()
    kode = results[-1].kode
    kode = int(kode) + 1
    kodeHotel = forms.CharField(label='Kode Hotel',max_length=20, initial=kode, disabled=(True))
    nama= forms.CharField(label='Nama Hotel',max_length=20)
    isRujukan= forms.BooleanField(label='Rujukan', required=(False))
    jalan=forms.CharField(label='Alamat : Jalan',max_length=50)
    kelurahan=forms.CharField(label='Kelurahan', max_length=20)
    kecamatan=forms.CharField(label='Kecamatan', max_length=20)
    kabkot=forms.CharField(label='Kabupaten/Kota', max_length=20)
    prov=forms.CharField(label='Provinsi', max_length=20)

class FormUpdateHotel(forms.Form):
    kodeHotel = forms.CharField(label='Kode Hotel',max_length=20, disabled=(True))
    nama= forms.CharField(label='Nama Hotel',max_length=20)
    isRujukan= forms.BooleanField(label='Rujukan', required=(False))
    jalan=forms.CharField(label='Alamat : Jalan',max_length=50)
    kelurahan=forms.CharField(label='Kelurahan', max_length=20)
    kecamatan=forms.CharField(label='Kecamatan', max_length=20)
    kabkot=forms.CharField(label='Kabupaten/Kota', max_length=20)
    prov=forms.CharField(label='Provinsi', max_length=20)