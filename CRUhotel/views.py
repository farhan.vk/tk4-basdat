from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *
from django.forms import inlineformset_factory, modelformset_factory
from django.contrib.auth import authenticate, login, logout
from django.db import DatabaseError, transaction
from collections import namedtuple
from django.utils.datastructures import MultiValueDictKeyError

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def updatehotel(request, kode_hotel):
    kode_hotel = str(kode_hotel)
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("SELECT * FROM HOTEL WHERE kode = '"+kode_hotel+"'")
    hasil = namedtuplefetchall(cursor)
    nama_hotel = hasil[0].nama
    jalan = hasil[0].jalan
    kelurahan = hasil[0].kelurahan
    kecamatan = hasil[0].kecamatan
    kabkot = hasil[0].kabkot
    prov = hasil[0].prov

    form = FormUpdateHotel(initial={
        'kodeHotel': kode_hotel, 'nama': nama_hotel, 'jalan': jalan, 'kelurahan' : kelurahan, 'kecamatan' : kecamatan,
        'kabkot' : kabkot, 'prov' : prov })
    
    if request.method == 'POST':
        nama_hotel = request.POST['nama']
        jalan = request.POST['jalan']
        kelurahan= request.POST['kelurahan']
        kecamatan = request.POST['kecamatan']
        kabkot= request.POST['kabkot']
        prov = request.POST['prov']

        try:
            isRujukan = request.POST['isRujukan']
        except MultiValueDictKeyError:
            isRujukan = False

        if isRujukan == "on" :
            isrujukan = "1"
        else :
            isrujukan = "0"

        try :
            cursor.execute("UPDATE HOTEL SET nama ='"+nama_hotel+"', isrujukan ='"+isrujukan+"', jalan ='"+jalan+"', kelurahan ='"+kelurahan+"', kecamatan ='"+kecamatan+"', kabkot ='"+kabkot+"', prov ='"+prov+"' WHERE kode ='"+kode_hotel+"'")
            cursor.close()
            return redirect('/list_hotel')
        except Exception as e:
            message = str(e)[80:]
            
    response = {
        'form' : form
    }
    return render(request, "update_hotel.html", response)


def listhotel(request):
    cursor = connection.cursor()
    roleAdmin = False

    try:
        if (request.session['role'] == 'admin'):
            roleAdmin = True
        
    except:
        return redirect('/')

    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("SELECT * FROM HOTEL")
    hasil = dictfetchall(cursor)
    cursor.close()
    response = {
        'list' : hasil,
        'roleAdmin' : roleAdmin,
    }
    return render(request, "list_hotel.html", response)

def createhotel(request):
    isRujukan = True
    try:
        if request.method == 'POST':
            registform = FormCreateHotel(request.POST)
            if registform.is_valid():
                kodeHotel = registform.cleaned_data['kodeHotel']
                nama= registform.cleaned_data['nama']
                jalan= registform.cleaned_data['jalan']
                kelurahan = registform.cleaned_data['kelurahan']
                kecamatan= registform.cleaned_data['kecamatan']
                kabkot = registform.cleaned_data['kabkot']
                prov= registform.cleaned_data['prov']

                try:
                    isRujukan = registform.cleaned_data['isRujukan']
                except MultiValueDictKeyError:
                    isRujukan = False
                
                if isRujukan == "on" :
                    isrujukan = "1"
                else :
                    isrujukan = "0"

                if check_hotel_is_exist(kodeHotel) == False:
                    with connection.cursor() as c:
                        c.execute("INSERT INTO HOTEL VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",[kodeHotel, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov])
                    return HttpResponseRedirect(reverse('CRUhotel:listhotel')) # CHANGE TO DESIRED APP
                else:
                    messages.error(request, 'Data hotel sudah terdaftar')
                    return HttpResponseRedirect('/list_hotel')
                    messages.error(request, 'Data paket makan sudah terdaftar')
    except DatabaseError:
        print("2") #cek terminal, masuk ga ini
        messages.error(request, "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu") # kenapa ga muncul ya
        return HttpResponseRedirect('/create_hotel')
    register_form = FormCreateHotel(request.POST)
    response = {'register_hotel':register_form}
    return render(request,'create_hotel.html',response)

def check_hotel_is_exist(kode_hotel):
    data = {}
    with connection.cursor() as c:
        c.execute("SET SEARCH_PATH TO SIRUCO")
        c.execute("SELECT * FROM DAFTAR_PESAN where kodehotel = %s",[kode_hotel])
        
        data = dictfetchall(c)
    return len(data) > 0 

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]