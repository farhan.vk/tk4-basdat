from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *
from datetime import datetime

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def reservasi_rs(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']

    if role != 'admin_satgas':
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for reservasi")

    else:
        if request.method == 'POST':
                registform = RegisterFormRS(request.POST)
                if registform.is_valid():
                    kodepasien = registform.cleaned_data['kodepasien']                        
                    tglmasuk = registform.cleaned_data['tglmasuk']
                    tglkeluar = registform.cleaned_data['tglkeluar']
                    koders = registform.cleaned_data['koders']
                    koderuangan = registform.cleaned_data['koderuangan']
                    kodebed = registform.cleaned_data['kodebed']
                    if check_reservasi_is_exist(kodepasien, tglmasuk) == False:
                        if tglkeluar < tglmasuk:
                            messages.error(request, 'Tanggal keluar tidak boleh sebelum tanggal masuk')
                            return HttpResponseRedirect('/reservasi_rs')

                        with connection.cursor() as c:
                            c.execute("INSERT INTO RESERVASI_RS VALUES (%s, %s, %s, %s, %s, %s, %s)",[kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed, "V0001"])
                        #    c.execute("SELECT * FROM PENGGUNA_PUBLIK")
                        #    data = dictfetchall(c)
                        #    print(data)
                        return HttpResponseRedirect(reverse('dashboard:dashboard')) # CHANGE TO DESIRED APP
                    else:
                        messages.error(request, 'Username/Email sudah terdaftar')
                        return HttpResponseRedirect('/reservasi_rs')
        register_form = RegisterFormRS()
        response = {'reservasi_rs':register_form}
        return render(request,'reservasi_rs.html',response)

def list_reservasi_rs(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']
    cursor_p = connection.cursor()

    if role == 'admin_satgas' or 'pengguna_publik':
        select="SELECT * FROM SIRUCO.RESERVASI_RS;"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        #print(data)
        if role =='admin_satgas':
            return render(request,'list_reservasi_rs.html',{ 'data':data})
        elif role == 'pengguna_publik':
            return render(request,'list_reservasi_rs_pp.html',{ 'data':data})
        else:
            return HttpResponseRedirect('/dashboard')
            messages.error("Unauthorized access for list reservasi")

def delete_reservasi(request, no, tgl):
    role= request.session['role']
    if role =='admin_satgas':
        with connection.cursor() as c:
            c.execute("SET search_path to SIRUCO")
            c.execute("DELETE FROM TRANSAKSI_RS where kodepasien = %s AND tglmasuk = %s",[no, tgl])
            c.execute("DELETE FROM RESERVASI_RS where kodepasien = %s AND tglmasuk = %s",[no, tgl])
        return HttpResponseRedirect('/list_reservasi_rs')
    else:
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for delete reservasi")

def update_reservasi(request, no, tgl):
    role= request.session['role']
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')

    if request.method == 'POST':
        kodepasien = request.POST.get('idpasien')
        tglmasuk = request.POST.get('indate')
        tglkeluar = request.POST.get('outdate')
        cursor_p = connection.cursor()
        select="UPDATE SIRUCO.RESERVASI_RS SET tglkeluar= '"+tglkeluar+"' WHERE kodepasien='"+no+"' AND tglmasuk='"+tgl+"';"
        change="UPDATE SIRUCO.TRANSAKSI_RS SET totalbiaya= abs('"+tglkeluar+"' :: date - '"+tgl+"' :: date)*500000 WHERE kodepasien='"+no+"' AND tglmasuk='"+tgl+"';"
        cursor_p.execute(select)
        cursor_p.execute(change)
        return HttpResponseRedirect('/list_reservasi_rs')
    
    else:
        username = request.session['username']
        role= request.session['role']
        if role == 'admin_satgas':
            cursor_p = connection.cursor()
            select="SELECT * FROM SIRUCO.RESERVASI_RS WHERE kodepasien='"+no+"' AND tglmasuk='"+tgl+"';"
            cursor_p.execute(select)
            data= dictfetchall(cursor_p)
            data_res = data[0]
            return render(request,'update_reservasi.html',{'data_res':data_res})
        else:
            return HttpResponseRedirect('/dashboard')
            messages.error("Unauthorized access for update reservasi")
            
def create_rs(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']

    if role != 'admin_satgas':
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for reservasi")

    else:
        if request.method == 'POST':
                registform = CreateRS(request.POST)
                isrujukan = request.POST.get('isrujukan')
                
                if isrujukan == "on":
                    isrujukan = "1"
                else:
                    isrujukan = "0"

                if registform.is_valid():
                    kode_faskes = registform.cleaned_data['kode_faskes']
                    # rujukan = registform.cleaned_data['rujukan']
                    if check_kode_faskes_is_exist("RUMAH_SAKIT", kode_faskes) == False:
                        with connection.cursor() as c:
                            c.execute("INSERT INTO RUMAH_SAKIT VALUES (%s, %s)",[kode_faskes, isrujukan])
                        return HttpResponseRedirect('/list_rs')
                    else:
                        messages.error(request, 'RS sudah terdaftar')
                        return HttpResponseRedirect('/create_rs')
        register_form = CreateRS()
        response = {'create_rs':register_form}
        return render(request,'create_rs.html',response)

def list_rs(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']
    cursor_p = connection.cursor()

    if role == 'admin_satgas':
        select="SELECT * FROM SIRUCO.RUMAH_SAKIT;"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        #print(data)
        return render(request,'list_rs.html',{ 'data':data})
    else:
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for list transaksi")

def update_rs(request, no):
    role= request.session['role']
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')

    if request.method == 'POST':
        kode_faskes = request.POST.get('kode_faskes')
        isrujukan = request.POST.get('isrujukan')
        if isrujukan == "on":
            isrujukan = "1"
            cursor_p = connection.cursor()
            select="UPDATE SIRUCO.RUMAH_SAKIT SET isrujukan= '"+isrujukan+"' WHERE kode_faskes ='"+no+"';"
            cursor_p.execute(select)
        else:
            isrujukan = "0"
            cursor_p = connection.cursor()
            select="UPDATE SIRUCO.RUMAH_SAKIT SET isrujukan= '"+isrujukan+"' WHERE kode_faskes ='"+no+"';"
            cursor_p.execute(select)

        return HttpResponseRedirect('/list_rs')
    
    else:
        username = request.session['username']
        role= request.session['role']
        if role == 'admin_satgas':
            cursor_p = connection.cursor()
            select="SELECT * FROM SIRUCO.RUMAH_SAKIT WHERE kode_faskes='"+no+"';"
            cursor_p.execute(select)
            data= dictfetchall(cursor_p)
            data_res = data[0]
            return render(request,'update_rs.html',{'data_res':data_res})
        else:
            return HttpResponseRedirect('/dashboard')
            messages.error("Unauthorized access for update reservasi")

def check_kode_faskes_is_exist(table, kode_faskes):
    data = {}

    with connection.cursor() as c:
        c.execute("SET search_path to SIRUCO")
        c.execute("SELECT * FROM %s where kode_faskes = %s " % (table, '%s'),[kode_faskes])
        data = dictfetchall(c)
    return len(data) > 0 

def check_reservasi_is_exist(kodepasien, tglmasuk):
    data = {}
    with connection.cursor() as c:
        c.execute("SET search_path to SIRUCO")
        c.execute("SELECT * FROM RESERVASI_RS where kodepasien = %s AND tglmasuk = %s",[kodepasien, tglmasuk])
        data = dictfetchall(c)
    return len(data) > 0 

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]