from django import forms
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime

class RegisterFormRS(forms.Form):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("""select nik from pasien""")
    results = cursor.fetchall()
    cursor.close()
    connection.close()
    nik = []
    for row in results:
        nik.append(row[0])
    
    daftar_nik = []
    for value in nik:
        daftar_nik.append((value,value))

    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("""select kode_faskes from rumah_sakit""")
    hasil = cursor.fetchall()
    cursor.close()
    connection.close()
    kode_rs = []
    for x in hasil:
        kode_rs.append(x[0])
    
    rscode = []
    for y in kode_rs:
        rscode.append((y,y))

    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("select koderuangan from ruangan_rs")
    res = cursor.fetchall()
    cursor.close()
    connection.close()
    kod = []

    for z in res:
        kod.append(z[0])

    roomcode = []
    for w in kod:
        roomcode.append((w,w))

    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("select distinct kodebed from bed_rs")
    rsult = cursor.fetchall()
    cursor.close()
    connection.close()
    bed = []
    for b in rsult:
        bed.append(b[0])

    bedcode = []
    for c in bed:
        bedcode.append((c,c))

    
    kodepasien = forms.ChoiceField(label=("NIK:"),choices = daftar_nik, widget = forms.Select({'class':'form-control'}))
    tglmasuk = forms.DateField(initial=datetime.date.today)
    tglkeluar = forms.DateField(initial=datetime.date.today)
    koders = forms.ChoiceField(label=("Kode RS:"),choices = rscode, widget = forms.Select({'class':'form-control'}))
    koderuangan = forms.ChoiceField(label=("Kode Ruangan:"),choices = roomcode, widget = forms.Select({'class':'form-control'}))
    kodebed = forms.ChoiceField(label=("Kode Bed:"),choices = bedcode, widget = forms.Select({'class':'form-control'}))

class UpdateResRS(forms.Form):
    tglmasuk = forms.DateField(disabled=True)

class CreateRS(forms.Form):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("""select kode from faskes""")
    results = cursor.fetchall()
    cursor.close()
    connection.close()
    cod = []
    for row in results:
        cod.append(row[0])
    
    kodefk = []
    for value in cod:
        kodefk.append((value,value))
    
    pilihan = []
    pilihan.append((0,0))
    pilihan.append((1,1))

    
    kode_faskes = forms.ChoiceField(label=("Kode:"),choices = kodefk, widget = forms.Select({'class':'form-control'}))
    #rujukan = forms.ChoiceField(label=("Rujukan:"),choices = pilihan, widget = forms.Select({'class':'form-control'}))