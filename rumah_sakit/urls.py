from django.urls import path
from .views import *

app_name = 'rumah_sakit'
urlpatterns = [
    path('reservasi_rs', reservasi_rs, name='reservasi_rs'),
    path('list_reservasi_rs', list_reservasi_rs, name='list_reservasi_rs'),
    path('delete_reservasi/<str:no>/<str:tgl>', delete_reservasi, name = 'delete_reservasi'),
    path('update_reservasi/<str:no>/<str:tgl>', update_reservasi, name = 'update_reservasi'),
    path('create_rs', create_rs, name='create_rs'),
    path('list_rs', list_rs, name='list_rs'),
    path('update_rs/<str:no>', update_rs, name='update_rs'),
]