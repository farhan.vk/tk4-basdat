from django.urls import path
from .views import *

app_name = 'CRUDpaketmakan'
urlpatterns = [
	path('create_paket_makan', createpaketmakan, name='createpaketmakan'),
    path('list_paket_makan', listpaketmakan, name='listpaketmakan'),
    path('delete_paket_makan/<str:kode_hotel>/<str:kode_paket>', deletepaketmakan, name='deletepaketmakan'),
    path('update_paket_makan/<str:kode_hotel>/<str:kode_paket>', updatepaketmakan, name='updatepaketmakan'),
]