from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *
from django.forms import inlineformset_factory, modelformset_factory
from django.contrib.auth import authenticate, login, logout
from django.db import DatabaseError, transaction
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def updatepaketmakan(request, kode_hotel, kode_paket):
    kode_hotel = str(kode_hotel)
    kode_paket = str(kode_paket)
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("SELECT * FROM PAKET_MAKAN WHERE kodehotel = '"+kode_hotel+"' and kodepaket = '"+kode_paket+"'")
    hasil = namedtuplefetchall(cursor)
    nama_paket = hasil[0].nama
    harga_paket = hasil[0].harga
    form = FormUpdatePaketMakan(initial={
        'kodeHotel': kode_hotel, 'kode_paket': kode_paket, 'nama_paket': nama_paket, 'harga' : harga_paket })
    
    if request.method == 'POST':
        nama_paket = request.POST['nama_paket']
        harga_paket= request.POST['harga']
        try :
            cursor.execute("UPDATE PAKET_MAKAN SET nama ='"+nama_paket+"', harga ='"+harga_paket+"' WHERE kodehotel ='"+kode_hotel+"' and kodepaket ='"+kode_paket+"'")
            cursor.close()
            return redirect('/list_paket_makan')
        except Exception as e:
            message = str(e)[80:]
            
    response = {
        'form' : form,
        'message' : message
    }
    return render(request, "update_paket_makan.html", response)


def deletepaketmakan(request, kode_hotel, kode_paket):
    kode_hotel = str(kode_hotel)
    kode_paket = str(kode_paket)
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    if check_paket_makan(kode_hotel, kode_paket) == True :
        return redirect('/list_paket_makan')
    else :   
        cursor.execute("DELETE FROM PAKET_MAKAN WHERE kodehotel = '"+kode_hotel+"' AND kodepaket = '"+kode_paket+"' ")
    cursor.close()
    messages.success(request, "Data PaketMakan berhasil terhapus")
    return HttpResponseRedirect(reverse('CRUDpaketmakan:listpaketmakan'))

def listpaketmakan(request):
    cursor = connection.cursor()
    roleAdmin = False

    try:
        if (request.session['role'] == 'admin'):
            roleAdmin = True
        
    except:
        return redirect('/')

    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("SELECT * FROM PAKET_MAKAN")
    hasil = dictfetchall(cursor)
    # cursor.execute("SELECT * FROM PAKET_MAKAN WHERE kodehotel in (SELECT kodehotel FROM DAFTAR_PESAN) AND kodepaket in (SELECT kodepaket FROM DAFTAR_PESAN)")
    # results = cursor.fetchall()
    # kode = []
    # for row in results:
    #     kode.append((row[0],row[1]))
    # print(kode)
    # cursor.execute("SELECT * FROM PAKET_MAKAN")
    # result = cursor.fetchall()
    # j = []
    # for i in result :
    #     j.append((i[0],i[1]))
    # print(j)
    cursor.close()
    response = {
        'list' : hasil,
        'roleAdmin' : roleAdmin,
        # 'kode' : kode
    }
    return render(request, "list_paket_makan.html", response)

def createpaketmakan(request):
    try:
        if request.method == 'POST':
            registform = FormCreatePaketMakan(request.POST)
            if registform.is_valid():
                kodeHotel = registform.cleaned_data['kodeHotel']
                kode_paket= registform.cleaned_data['kode_paket']
                nama_paket = registform.cleaned_data['nama_paket']
                harga= registform.cleaned_data['harga']

                if check_paket_makan_is_exist(kodeHotel, kode_paket) == False:
                    with connection.cursor() as c:
                        c.execute("INSERT INTO PAKET_MAKAN VALUES (%s, %s, %s, %s)",[kodeHotel, kode_paket, nama_paket, harga])
                    return HttpResponseRedirect(reverse('CRUDpaketmakan:listpaketmakan')) # CHANGE TO DESIRED APP
                else:
                    messages.error(request, 'Data paket makan sudah terdaftar')
                    return HttpResponseRedirect('/list_paket_makan')
                    messages.error(request, 'Data paket makan sudah terdaftar')
    except DatabaseError:
        print("2") #cek terminal, masuk ga ini
        messages.error(request, "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu") # kenapa ga muncul ya
        return HttpResponseRedirect('/create_pasien_makan')
    register_form = FormCreatePaketMakan(request.POST)
    response = {'register_paket_makan':register_form}
    return render(request,'create_paket_makan.html',response)

def check_paket_makan_is_exist(kode_hotel, kode_paket):
    data = {}
    with connection.cursor() as c:
        c.execute("SET SEARCH_PATH TO SIRUCO")
        c.execute("SELECT * FROM PAKET_MAKAN where kodehotel = %s AND kodepaket = %s",[kode_hotel, kode_paket])
        
        data = dictfetchall(c)
    return len(data) > 0 

def check_paket_makan(kode_hotel, kode_paket):
    data = {}
    with connection.cursor() as c:
        c.execute("SET SEARCH_PATH TO SIRUCO")
        c.execute("SELECT * FROM DAFTAR_PESAN where kodehotel = %s AND kodepaket = %s",[kode_hotel, kode_paket])
        
        data = dictfetchall(c)
    return len(data) > 0 

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]




