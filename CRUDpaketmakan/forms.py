from django import forms
from django.db import connection

class FormCreatePaketMakan(forms.Form):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("""SELECT KodeHotel FROM HOTEL_ROOM""")
    results = cursor.fetchall()
    cursor.close()
    connection.close()
    kode = []
    for row in results:
        kode.append(row[0])
    
    kode_hotel = []
    for value in kode:
        kode_hotel.append((value,value))

    kodeHotel = forms.ChoiceField(label=("Kode Hotel:"),
                                    choices = kode_hotel,
                                    widget = forms.Select({'class':'form-control'}))
    kode_paket= forms.CharField(label='Kode Paket',max_length=20)
    nama_paket=forms.CharField(label='Nama Paket',max_length=50)
    harga=forms.CharField(label='Harga', max_length=20)

class FormUpdatePaketMakan(forms.Form):
    kodeHotel = forms.CharField(label='Kode Hotel',max_length=20, disabled=(True))
    kode_paket = forms.CharField(label='Kode Paket',max_length=20, disabled=(True))
    nama_paket = forms.CharField(label='Nama Paket',max_length=50)
    harga = forms.CharField(label='Harga', max_length=20)
