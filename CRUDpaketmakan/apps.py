from django.apps import AppConfig


class CrudpaketmakanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CRUDpaketmakan'
