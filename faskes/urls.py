from django.urls import path
from .views import *

app_name = 'faskes'
urlpatterns = [
    path('buat_jadwal', buat_jadwal, name='buat_jadwal'),
    path('list_jadwal', list_jadwal, name='list_jadwal'),
    path('list_faskes', list_faskes, name='list_faskes'),
    path('update_faskes/<str:no>', update_faskes, name = 'update_faskes'),
    path('delete_faskes/<str:no>', delete_faskes, name = 'delete_faskes'),
]