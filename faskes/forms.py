from django import forms
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime

class CreateJadwal(forms.Form):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("""select kode from faskes""")
    results = cursor.fetchall()
    cursor.close()
    connection.close()
    cod = []
    for row in results:
        cod.append(row[0])
    
    kodefk = []
    for value in cod:
        kodefk.append((value,value))

    kode_faskes = forms.ChoiceField(label=("Kode:"),choices = kodefk, widget = forms.Select({'class':'form-control'}))
    shift = forms.CharField(label='Shift',max_length=15)

