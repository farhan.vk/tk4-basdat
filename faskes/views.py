from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *
from datetime import datetime

def buat_jadwal(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']

    if role != 'admin_satgas':
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for pembuatan jadwal")

    else:
        if request.method == 'POST':
                registform = CreateJadwal(request.POST)
                tanggal = request.POST.get('tanggal')

                if registform.is_valid():
                    kode_faskes = registform.cleaned_data['kode_faskes']   
                    shift = registform.cleaned_data['shift']
                    if check_jadwal_is_exist(kode_faskes, shift, tanggal    ) == False:
                        with connection.cursor() as c:
                            c.execute("INSERT INTO SIRUCO.JADWAL VALUES (%s, %s, %s)",[kode_faskes, shift, tanggal])
                        return HttpResponseRedirect('/list_rs')
                    else:
                        messages.error(request, 'RS sudah terdaftar')
                        return HttpResponseRedirect('/create_rs')
        register_form = CreateJadwal()
        response = {'buat_jadwal':register_form}
        return render(request,'buat_jadwal.html',response)

def list_jadwal(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']
    cursor_p = connection.cursor()

    if role == 'admin_satgas':
        select="SELECT * FROM SIRUCO.JADWAL;"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        #print(data)
        return render(request,'list_jadwal.html',{ 'data':data})
    else:
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for list jadwal")    

def list_faskes(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']
    cursor_p = connection.cursor()

    if role == 'admin_satgas':
        select="SELECT * FROM SIRUCO.FASKES;"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        #print(data)
        return render(request,'list_faskes.html',{ 'data':data})
    else:
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for list faskes")  

def update_faskes(request, no):
    role= request.session['role']
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')

    if request.method == 'POST':
        tipe = request.POST.get('tipe')
        nama = request.POST.get('nama')
        statusmilik = request.POST.get('statusmilik')
        jalan = request.POST.get('jalan')
        kelurahan = request.POST.get('kelurahan')
        kecamatan = request.POST.get('kecamatan')
        kabkot = request.POST.get('kabkot')
        prov = request.POST.get('prov')
        #print(nama)
        #print("---------------------")

        #cursor_p = connection.cursor()
        #select="UPDATE SIRUCO.FASKES SET tipe= '"+tipe+"' WHERE kode='"+no+"';"
        #select1 = "UPDATE SIRUCO.FASKES SET nama = '"+nama+"' WHERE kode='"+no+"';"
        #select2 = "UPDATE SIRUCO.FASKES SET statusmilik = '"+statusmilik+"' WHERE kode='"+no+"';"
        #select3 = "UPDATE SIRUCO.FASKES SET jalan = '"+jalan+"' WHERE kode='"+no+"';"
        #select4 = "UPDATE SIRUCO.FASKES SET kelurahan = '"+kelurahan+"' WHERE kode='"+no+"';"
        #select5 = "UPDATE SIRUCO.FASKES SET kecamatan = '"+kecamatan+"' WHERE kode='"+no+"';"
        #select6 = "UPDATE SIRUCO.FASKES SET kabkot = '"+kabkot+"' WHERE kode='"+no+"';"
        #select7 = "UPDATE SIRUCO.FASKES SET prov = '"+prov+"' WHERE kode='"+no+"';"
        #cursor_p.execute(select)
        #cursor_p.execute(select1)
        #cursor_p.execute(select2)
        #cursor_p.execute(select3)
        #cursor_p.execute(select4)
        #cursor_p.execute(select5)
        #cursor_p.execute(select6)
        #cursor_p.execute(select7)
        return HttpResponseRedirect('/list_faskes')
    
    else:
        username = request.session['username']
        role= request.session['role']
        if role == 'admin_satgas':
            cursor_p = connection.cursor()
            select="SELECT * FROM SIRUCO.FASKES WHERE kode='"+no+"';"
            cursor_p.execute(select)
            data= dictfetchall(cursor_p)
            data_res = data[0]
            return render(request,'update_faskes.html',{'data_res':data_res})
        else:
            return HttpResponseRedirect('/dashboard')
            messages.error("Unauthorized access for update faskes")

def delete_faskes(request, no):
    role= request.session['role']
    if role =='admin_satgas':
        with connection.cursor() as c:
            c.execute("DELETE FROM SIRUCO.FASKES where kode = %s",[no])
        return HttpResponseRedirect('/list_faskes')
    else:
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for delete faskes")

    
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def check_kode_faskes_is_exist(table, kode_faskes):
    data = {}

    with connection.cursor() as c:
        c.execute("SET search_path to SIRUCO")
        c.execute("SELECT * FROM %s where kode_faskes = %s " % (table, '%s'),[kode_faskes])
        data = dictfetchall(c)
    return len(data) > 0 

def check_jadwal_is_exist(kode_faskes, shift, tanggal):
    data = {}

    cursor_p = connection.cursor()
    select="SELECT * FROM SIRUCO.JADWAL where kode_faskes = '"+kode_faskes+"' AND shift='"+shift+"' AND tanggal='"+tanggal+"';"
    cursor_p.execute(select)
    data = dictfetchall(cursor_p)
    return len(data) > 0 
