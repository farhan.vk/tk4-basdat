from django.urls import path
from .views import *

app_name = 'jadwaldokter'
urlpatterns = [
	path('read_jadwaldokter', readjadwaldokter, name='read_jadwaldokter'),
]