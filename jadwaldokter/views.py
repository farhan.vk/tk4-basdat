from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.db import connection
from collections import namedtuple


# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
    
def readjadwaldokter (request, feedback=None):
    
    cursor = connection.cursor()
    if (request.session['role'] == 'pengguna_publik') or (request.session['role'] == 'admin_satgas'):
        x ="SELECT * from siruco.jadwal_dokter"
    elif (request.session['role'] == 'dokter'):
        x = "SELECT * FROM SIRUCO.jadwal_dokter where username='"+request.session['username']+"'"
    cursor.execute(x)
    cek = namedtuplefetchall(cursor)

    context = {
        'cek':cek
    }
    return render(request, "Bacajadwaldokter.html", context)
