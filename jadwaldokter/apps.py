from django.apps import AppConfig


class JadwaldokterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jadwaldokter'
