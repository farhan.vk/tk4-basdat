from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.db import connection
from collections import namedtuple


# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
    
def readruanganRS (request, feedback=None):
    
    cursor = connection.cursor()
    sql ="SELECT * from siruco.ruangan_rs"
    
    cursor.execute(sql)
    cek = namedtuplefetchall(cursor)

    context = {
        'cek':cek
    }
    return render(request, "BacaruanganRS.html", context)

def readbedRS (request, feedback=None):
    
    cursor = connection.cursor()
    x ="SELECT * from siruco.bed_rs"
    cursor.execute(x)
    cek = namedtuplefetchall(cursor)
    print(cek)
    context = {
        'cek':cek
    }
    return render(request, "BacabedRS.html", context)

def deletebedRS(request, koders,koderuangan,kodebed):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("DELETE FROM bed_rs WHERE koders = %s AND koderuangan=%s AND kodebed=%s",[koders, koderuangan, kodebed])
    cursor.close()
    message = "Bed rs berhasil terhapus"
    response = {'message' : message}
    return redirect('ruanganbedRS:readbedRS')
