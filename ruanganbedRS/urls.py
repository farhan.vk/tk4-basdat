from django.urls import path
from .views import *

app_name = 'ruanganbedRS'
urlpatterns = [
    path('read_ruanganRS', readruanganRS, name='read_ruanganRS'),
	path('read_bedRS', readbedRS, name='read_bedRS'),
    path('read_bedRS/<str:koders>/<str:koderuangan>/<str:kodebed>', deletebedRS, name='delete_bedRS'),
]