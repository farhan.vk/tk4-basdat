from django.apps import AppConfig


class CrudpasienConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CRUDpasien'
