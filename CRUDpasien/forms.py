from django import forms
from django.db import connection

class FormCreatePasien(forms.Form):
    nik=forms.CharField(label='NIK',max_length=50)
    nama= forms.CharField(label='Nama',max_length=20)
    nomor_telepon=forms.CharField(label='Nomor Telepon',max_length=50)
    nomor_hp=forms.CharField(label='Nomor HP', max_length=20)
    jalan_ktp=forms.CharField(label='Alamat(Sesuai KTP) Jalan',max_length=50)
    kel_ktp= forms.CharField(label='Kelurahan', max_length=20)
    kec_ktp=forms.CharField(label='Kecamatan',max_length=50)
    kab_ktp=forms.CharField(label='Kabupaten', max_length=20)
    prov_ktp=forms.CharField(label='Provinsi', max_length=12)
    jalan_dom=forms.CharField(label='Aalamat (Domisili) Jalan',max_length=50)
    kel_dom= forms.CharField(label='Kelurahan', max_length=20)
    kec_dom=forms.CharField(label='Kecamatan',max_length=50)
    kab_dom=forms.CharField(label='Kabupaten', max_length=20)
    prov_dom=forms.CharField(label='Provinsi', max_length=12)

class UpdateFormPasien(forms.Form):
    nik=forms.CharField(label='NIK',max_length=50, disabled=True)
    nama= forms.CharField(label='Nama',max_length=20, disabled=True)
    nomor_telepon=forms.CharField(label='Nomor Telepon',max_length=50)
    nomor_hp=forms.CharField(label='Nomor HP', max_length=20)
    jalan_ktp=forms.CharField(label='Alamat(Sesuai KTP) Jalan',max_length=50)
    kel_ktp= forms.CharField(label='Kelurahan', max_length=20)
    kec_ktp=forms.CharField(label='Kecamatan',max_length=50)
    kab_ktp=forms.CharField(label='Kabupaten', max_length=20)
    prov_ktp=forms.CharField(label='Provinsi', max_length=12)
    jalan_dom=forms.CharField(label='Aalamat (Domisili) Jalan',max_length=50)
    kel_dom= forms.CharField(label='Kelurahan', max_length=20)
    kec_dom=forms.CharField(label='Kecamatan',max_length=50)
    kab_dom=forms.CharField(label='Kabupaten', max_length=20)
    prov_dom=forms.CharField(label='Provinsi', max_length=12)


