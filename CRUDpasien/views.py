from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *
from django.forms import inlineformset_factory, modelformset_factory
from django.contrib.auth import authenticate, login, logout
from django.db import DatabaseError, transaction
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def listpasien(request):
    username = request.session['username']
    cursor = connection.cursor()
    rolePenggunaPublik = False

    try:
        if (request.session['role'] == 'pengguna_publik'):
            rolePenggunaPublik = True
    except:
        return redirect('/')

    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("SELECT * FROM PASIEN WHERE idPendaftar = '"+username+"'")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    response = {
        'list' : hasil,
        'rolePenggunaPublik' : rolePenggunaPublik,
    }
    return render(request, "list_pasien.html", response)

def updatepasien(request, nik, success=True):
    message =""
    nik = str(nik)
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("SELECT * FROM PASIEN WHERE nik = '"+nik+"'")
    hasil = namedtuplefetchall(cursor)
    nik = hasil[0].nik
    pendaftar = hasil[0].idpendaftar
    nama = hasil[0].nama
    nomor_telepon = hasil[0].notelp
    nomor_hp = hasil[0].nohp
    ktp_jalan = hasil[0].ktp_jalan
    ktp_kelurahan = hasil[0].ktp_kelurahan
    ktp_kecamatan = hasil[0].ktp_kecamatan
    ktp_kabkot = hasil[0].ktp_kabkot
    ktp_prov= hasil[0].ktp_prov
    dom_jalan = hasil[0].dom_jalan
    dom_kelurahan = hasil[0].dom_kelurahan
    dom_kecamatan = hasil[0].dom_kecamatan
    dom_kabkot = hasil[0].dom_kabkot
    dom_prov= hasil[0].dom_prov
    form = UpdateFormPasien(initial={
        'nik': nik, 'pendaftar': pendaftar, 'nama': nama, 'nomor_telepon': nomor_telepon, 'nomor_hp': nomor_hp, 'jalan_ktp': ktp_jalan,
        'kel_ktp': ktp_kelurahan, 'kec_ktp': ktp_kecamatan, 'kab_ktp': ktp_kabkot, 'prov_ktp': ktp_prov, 'jalan_dom': dom_jalan, 'kel_dom': dom_kelurahan,
        'kec_dom': dom_kecamatan, 'kab_dom': dom_kabkot, 'prov_dom': dom_prov})
    
    if request.method == 'POST':
        nomor_telepon = request.POST['nomor_telepon']
        nomor_hp= request.POST['nomor_hp']
        jalan_ktp = request.POST['jalan_ktp']
        kel_ktp = request.POST['kel_ktp']
        kec_ktp = request.POST['kec_ktp']
        kab_ktp = request.POST['kab_ktp']
        prov_ktp = request.POST['prov_ktp']
        jalan_dom = request.POST['jalan_dom']
        kel_dom = request.POST['kel_dom']
        kec_dom = request.POST['kec_dom']
        kab_dom = request.POST['kab_dom']
        prov_dom = request.POST['prov_dom']

        try :
            with connection.cursor() as c:
                c.execute("UPDATE PASIEN SET notelp ='"+nomor_telepon+"', nohp ='"+nomor_hp+"', ktp_jalan ='"+jalan_ktp+"', ktp_kecamatan ='"+kec_ktp+"', ktp_kelurahan ='"+kel_ktp+"', ktp_kabkot ='"+kab_ktp+"', ktp_prov ='"+prov_ktp+"', dom_jalan ='"+jalan_dom+"', dom_kecamatan ='"+kec_dom+"', dom_kelurahan ='"+kel_dom+"', dom_kabkot ='"+kab_dom+"', dom_prov ='"+prov_dom+"' WHERE nik ='"+nik+"'")
                cursor.close()
            return HttpResponseRedirect('/list_pasien')
        except Exception as e:
            message = str(e)[80:]
    username = request.session['username']        
    response = {
        'form' : form,
        'message' : message,
        'username' : username
    }
    return render(request, "update_pasien.html", response)

def deletepasien(request, nik):
    nik = str(nik)
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("DELETE FROM PASIEN WHERE nik = '"+nik+"'")
    cursor.close()
    messages.success(request, "Data pasien berhasil terhapus")
    return HttpResponseRedirect(reverse('CRUDpasien:listpasien'))


def detailpasien(request, nik):
    nik = str(nik)
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIRUCO")
    cursor.execute("SELECT * FROM PASIEN WHERE nik = '"+nik+"'")
    hasil = dictfetchall(cursor)
    cursor.close()
    response = {'detail' : hasil}
    return render(request, "detail_pasien.html", response)

def createpasien(request):
    try:
        if request.method == 'POST':
            registform = FormCreatePasien(request.POST)
            if registform.is_valid():
                nik = registform.cleaned_data['nik']
                nama= registform.cleaned_data['nama']
                nomor_telepon = registform.cleaned_data['nomor_telepon']
                nomor_hp= registform.cleaned_data['nomor_hp']
                jalan_ktp = registform.cleaned_data['jalan_ktp']
                kel_ktp = registform.cleaned_data['kel_ktp']
                kec_ktp = registform.cleaned_data['kec_ktp']
                kab_ktp = registform.cleaned_data['kab_ktp']
                prov_ktp = registform.cleaned_data['prov_ktp']
                jalan_dom = registform.cleaned_data['jalan_dom']
                kel_dom = registform.cleaned_data['kel_dom']
                kec_dom = registform.cleaned_data['kec_dom']
                kab_dom = registform.cleaned_data['kab_dom']
                prov_dom = registform.cleaned_data['prov_dom']

                if check_pasien_is_exist(nama,nik) == False:
                    with connection.cursor() as c:
                        c.execute("INSERT INTO PASIEN VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",[nik, request.session['username'], nama, jalan_ktp, kel_ktp, kec_ktp, kab_ktp, prov_ktp, jalan_dom, kel_dom, kec_dom, kab_dom, prov_dom, nomor_telepon, nomor_hp])
                    return HttpResponseRedirect(reverse('CRUDpasien:listpasien')) # CHANGE TO DESIRED APP
                else:
                    messages.error(request, 'Data pasien sudah terdaftar')
                    return HttpResponseRedirect('/list_pasien')
                    messages.error(request, 'Data pasien sudah terdaftar')
    except DatabaseError:
        print("2") #cek terminal, masuk ga ini
        messages.error(request, "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu") # kenapa ga muncul ya
        return HttpResponseRedirect('/register_pasien')
    register_form = FormCreatePasien(request.POST)
    username = request.session['username']
    response = {'register_pasien':register_form,
                'username':username}
    return render(request,'create_pasien.html',response)

def check_pasien_is_exist(nama, nik):
    data = {}
    with connection.cursor() as c:
        c.execute("SET SEARCH_PATH TO SIRUCO")
        c.execute("SELECT * FROM PASIEN where nama = %s AND nik = %s",[nama, nik])
        
        data = dictfetchall(c)
    return len(data) > 0 

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]