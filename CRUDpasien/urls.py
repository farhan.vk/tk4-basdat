from django.urls import path
from .views import *

app_name = 'CRUDpasien'
urlpatterns = [
	path('register_pasien', createpasien, name='createpasien'),
    path('list_pasien', listpasien, name='listpasien'),
    path('detail_pasien/<str:nik>', detailpasien, name='detailpasien'),
    path('delete_pasien/<str:nik>', deletepasien, name='deletepasien'),
    path('update_pasien/<str:nik>', updatepasien, name='updatepasien'),
]