from django.urls import path
from .views import *

app_name = 'transaksi'
urlpatterns = [
    path('list_transaksi_rs', list_transaksi_rs, name='list_transaksi_rs'),
    path('delete_transaksi/<str:no>', delete_transaksi, name = 'delete_transaksi'),
    path('update_transaksi/<str:no>', update_transaksi, name = 'update_transaksi'),
]