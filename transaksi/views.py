from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

def list_transaksi_rs(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']
    cursor_p = connection.cursor()

    if role == 'admin_satgas':
        select="SELECT * FROM SIRUCO.TRANSAKSI_RS;"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        return render(request,'list_transaksi_rs.html',{ 'data':data})
    else:
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for list transaksi")

def delete_transaksi(request, no):
    role= request.session['role']
    if role =='admin_satgas':
        with connection.cursor() as c:
            c.execute("SET search_path to SIRUCO")
            c.execute("DELETE FROM TRANSAKSI_RS where idtransaksi = %s",[no])
        return HttpResponseRedirect('/list_transaksi_rs')
    else:
        return HttpResponseRedirect('/dashboard')
        messages.error("Unauthorized access for delete transaksi")

def update_transaksi(request, no):
    role= request.session['role']
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')

    if request.method == 'POST':
        statusbayar = request.POST.get('statusbayar')
        cursor_p = connection.cursor()
        select="UPDATE SIRUCO.TRANSAKSI_RS SET statusbayar= '"+statusbayar+"' WHERE idtransaksi='"+no+"';"
        cursor_p.execute(select)
        return HttpResponseRedirect('/list_transaksi_rs')
    
    else:
        username = request.session['username']
        role= request.session['role']
        if role == 'admin_satgas':
            cursor_p = connection.cursor()
            select="SELECT * FROM SIRUCO.TRANSAKSI_RS WHERE idtransaksi ='"+no+"';"
            cursor_p.execute(select)
            data= dictfetchall(cursor_p)
            data_res = data[0]
            return render(request,'update_transaksi.html',{'data_res':data_res})
        else:
            return HttpResponseRedirect('/dashboard')
            messages.error("Unauthorized access for update reservasi")

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]