from django.urls import path
from .views import *

app_name = 'login'
urlpatterns = [
	path('register_admin', register_admin, name='register_admin'),
	path('register_penggunapublik',register_penggunapublik, name='register_penggunapublik'),
    path('register_dokter',register_dokter, name='register_dokter'),
    path('register_adminsatgas',register_adminsatgas, name='register_adminsatgas'),
    path('login_user', login_user, name='login_user'),
    path('logout_user', logout_user, name='logout_user')
]