from django import forms
from django.db import connection

class FormLogin(forms.Form):
    username=forms.CharField(label='Username',max_length=50)
    password=forms.CharField(label='Password',widget=forms.PasswordInput, max_length=20)

class DaftarAdminSatgas(forms.Form):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("""select kode from faskes""")
    hasil = cursor.fetchall()
    cursor.close()
    connection.close()
    kode = []
    for x in hasil:
        kode.append(x[0])
    
    kodef = []
    for value in kode:
        kodef.append((value,value))

    username=forms.CharField(label="Username",max_length=50)
    password= forms.CharField(label='Password',widget=forms.PasswordInput, max_length=20)
    idfaskes = forms.ChoiceField(label=("Kode:"), choices = kodef, widget = forms.Select({'class':'form-control'}))
class DaftarFormDokter(forms.Form):
    username=forms.CharField(label='Email / Username',max_length=50)
    password= forms.CharField(label='Password',widget=forms.PasswordInput, max_length=20)
    noSTR=forms.CharField(label='Nomor STR', max_length=20)
    nama=forms.CharField(label='Nama',max_length=50)
    no_hp=forms.CharField(label='Nomor HP', max_length=12)
    gelar_depan=forms.CharField(label='Gelar Depan', max_length=10)
    gelar_belakang=forms.CharField(label='Gelar Belakang', max_length=10)
class DaftarAdminSistem(forms.Form):
    username=forms.CharField(label='Username',max_length=50)
    password= forms.CharField(label='Password',widget=forms.PasswordInput, max_length=20)


class DaftarPenggunaPublik(forms.Form):
    username=forms.CharField(label='Email / Username',max_length=50)
    password= forms.CharField(label='Password',widget=forms.PasswordInput, max_length=20)
    nama=forms.CharField(label='Nama',max_length=50)
    nik=forms.CharField(label='NIK', max_length=20)
    no_hp=forms.CharField(label='Nomor Telepon', max_length=12)


