from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *
from django.forms import inlineformset_factory, modelformset_factory
from django.contrib.auth import authenticate, login, logout
from django.db import DatabaseError, transaction

# Create your views here.
def register_admin(request):
    if request.method == 'POST':
        registform = DaftarAdminSistem(request.POST)
        try:
            if registform.is_valid():
                username = registform.cleaned_data['username']
                password = registform.cleaned_data['password']
                if check_user_is_exist("AKUN_PENGGUNA",username) == False:
                    with connection.cursor() as c:
                        c.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)",[username, password, "admin"])
                        c.execute("INSERT INTO ADMIN VALUES (%s)",[username])
                        #c.execute("SELECT * FROM ADMIN")
                        #data = dictfetchall(c)           
                        #print(data)        
                        request.session['username'] = username
                        request.session['password'] = password
                        request.session['role'] = 'admin'
                        return HttpResponseRedirect(reverse('dashboard:dashboard')) # CHANGE TO DESIRED APP
                else:
                    print("1") #cek terminal, masuk ga ini
                    messages.error(request, "Username taken") # kenapa ga muncul ya
                    return HttpResponseRedirect('/register_admin')
        except DatabaseError:
            print("2") #cek terminal, masuk ga ini
            messages.error(request, "Password harus memiliki minimal 1 huruf kapital dan 1 angka") # kenapa ga muncul ya
            return HttpResponseRedirect('/register_admin')

    register_form = DaftarAdminSistem
    response = {'register_admin': register_form}
    return render(request,'register_admin.html',response)

def register_penggunapublik(request):
    try:
        if request.method == 'POST':
            registform = DaftarPenggunaPublik(request.POST)
            if registform.is_valid():
                username = registform.cleaned_data['username']
                password = registform.cleaned_data['password']
                nama = registform.cleaned_data['nama']
                nik = registform.cleaned_data['nik']
                no_hp = registform.cleaned_data['no_hp']
                if check_user_is_exist("AKUN_PENGGUNA",username) == False:
                    with connection.cursor() as c:
                        c.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)",[username, password, "pengguna publik"])
                        c.execute("INSERT INTO PENGGUNA_PUBLIK VALUES (%s, %s, %s, %s, %s, %s)",[username, nik, nama, "aktif", "pasien", no_hp])
                        c.execute("SELECT * FROM PENGGUNA_PUBLIK")
                        data = dictfetchall(c)           
                        print(data)
                    request.session['username'] = username
                    request.session['password'] = password
                    request.session['role'] = 'pengguna_publik'
                    return HttpResponseRedirect(reverse('dashboard:dashboard')) # CHANGE TO DESIRED APP
                else:
                    messages.error(request, 'Username/Email sudah terdaftar')
                    return HttpResponseRedirect('/register_penggunapublik')
    except DatabaseError:
        print("2") #cek terminal, masuk ga ini
        messages.error(request, "Password harus memiliki minimal 1 huruf kapital dan 1 angka") # kenapa ga muncul ya
        return HttpResponseRedirect('/register_penggunapublik')
    register_form = DaftarPenggunaPublik
    response = {'register_penggunapublik':register_form}
    return render(request,'register_penggunapublik.html',response)

def register_dokter(request):
    try:
        if request.method == 'POST':
            registform = DaftarFormDokter(request.POST)
            if registform.is_valid():
                username = registform.cleaned_data['username']
                password = registform.cleaned_data['password']
                nostr = registform.cleaned_data['noSTR']
                nama = registform.cleaned_data['nama']
                no_hp = registform.cleaned_data['no_hp']
                gelar_depan = registform.cleaned_data['gelar_depan']
                gelar_belakang = registform.cleaned_data['gelar_belakang']

                if check_user_is_exist("AKUN_PENGGUNA",username) or check_dokter_is_exist(username, nostr) == False:
                    with connection.cursor() as c:
                        c.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)",[username, password, "admin"])
                        c.execute("INSERT INTO ADMIN VALUES (%s)",[username])
                        c.execute("INSERT INTO DOKTER VALUES (%s, %s, %s, %s, %s, %s)",[username, nostr, nama, no_hp, gelar_depan, gelar_belakang])
                    request.session['username'] = username
                    request.session['nostr'] = nostr
                    request.session['password'] = password
                    request.session['role'] = 'dokter'
                    return HttpResponseRedirect(reverse('dashboard:dashboard')) # CHANGE TO DESIRED APP
                else:
                    messages.error(request, 'Username/Email atau kombinasi Username dan NoSTR sudah terdaftar')
                    return HttpResponseRedirect('/register_dokter')
                    messages.error(request, 'Username/Email atau kombinasi Username dan NoSTR sudah terdaftar')
    except DatabaseError:
        print("2") #cek terminal, masuk ga ini
        messages.error(request, "Password harus memiliki minimal 1 huruf kapital dan 1 angka") # kenapa ga muncul ya
        return HttpResponseRedirect('/register_dokter')
    register_form = DaftarFormDokter()
    response = {'register_dokter':register_form}
    return render(request,'register_dokter.html',response)

def register_adminsatgas(request):
    try:
        if request.method == 'POST':
            registform = DaftarAdminSatgas(request.POST)
            if registform.is_valid():
                username = registform.cleaned_data['username']
                password = registform.cleaned_data['password']
                idfaskes = registform.cleaned_data['idfaskes']

                if check_user_is_exist("AKUN_PENGGUNA",username) == False:
                    with connection.cursor() as c:
                        c.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)",[username, password, "admin"])
                        c.execute("INSERT INTO ADMIN VALUES (%s)",[username])
                        c.execute("INSERT INTO ADMIN_SATGAS VALUES (%s, %s)",[username, idfaskes])
                    request.session['username'] = username
                    request.session['password'] = password
                    request.session['role'] = 'admin_satgas'
                    return HttpResponseRedirect(reverse('dashboard:dashboard')) # CHANGE TO DESIRED APP
                else:
                    messages.error(request, 'Username/Email sudah terdaftar')
                    return HttpResponseRedirect('/register_adminsatgas')
    except DatabaseError:
        print("2") #cek terminal, masuk ga ini
        messages.error(request, "Password harus memiliki minimal 1 huruf kapital dan 1 angka") # kenapa ga muncul ya
        return HttpResponseRedirect('/register_adminsatgas')
    register_form = DaftarAdminSatgas()
    response = {'register_adminsatgas':register_form}
    return render(request,'register_adminsatgas.html',response)

def login_user(request):
    if 'username' in request.session:
        return HttpResponseRedirect('/dashboard')
    if request.method == 'POST':
        loginform = FormLogin(request.POST)
        if loginform.is_valid():
            username = loginform.cleaned_data['username']
            password = loginform.cleaned_data['password']
            if check_user_is_exist("AKUN_PENGGUNA",username,password):
                request.session['username'] = username
                request.session['password'] = password
                role_list = ["dokter", "admin_satgas", "pengguna_publik", "admin"]
                for role in role_list:
                    if check_user_is_exist(role,username):
                        request.session['role'] = role.lower()
                        break
                return HttpResponseRedirect('/dashboard') # CHANGE TO DESIRED APP
            else:
                messages.error(request, 'Email/Password salah')
                return HttpResponseRedirect('/login_user')
    loginform = FormLogin
    response = {'loginform':loginform}
    return render(request,'login_user.html',response)

def logout_user(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    request.session.flush()
    return HttpResponseRedirect(reverse('homepage:index'))



def check_user_is_exist(table, username, password = None):
    data = {}
    if password is not None :
        with connection.cursor() as c:
            c.execute("SET search_path to SIRUCO")
            c.execute("SELECT * FROM %s where username = %s and password = %s" % (table, '%s', '%s'),[username,password])
            data = dictfetchall(c)
    else:
        with connection.cursor() as c:
            c.execute("SET search_path to SIRUCO")
            c.execute("SELECT * FROM %s where username = %s " % (table, '%s'),[username])
            data = dictfetchall(c)
    return len(data) > 0 


def check_dokter_is_exist(username, nostr):
    data = {}
    with connection.cursor() as c:
        c.execute("SET search_path to SIRUCO")
        c.execute("SELECT * FROM DOKTER where username = %s AND nostr = %s",[username, nostr])
        
        data = dictfetchall(c)
    return len(data) > 0 


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
