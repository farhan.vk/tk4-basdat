from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def dashboard(request):
    if 'username' not in request.session:
        return HttpResponseRedirect('/login_user')
    username = request.session['username']
    role= request.session['role']
    cursor_p = connection.cursor()

    select = "SELECT * FROM SIRUCO.AKUN_PENGGUNA WHERE username='"+username+"';"
    cursor_p.execute(select)
    data = namedtuplefetchall(cursor_p)
    general_data = data[0]

    if role == 'pengguna_publik':
        select="SELECT * FROM SIRUCO.PENGGUNA_PUBLIK WHERE username='"+username+"';"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        data_pengguna_publik = data[0]
        return render(request,'dashboard_pengguna_publik.html',{'general_data':general_data,
        'data_pengguna_publik':data_pengguna_publik})

    elif role =='admin':
        select="SELECT * FROM SIRUCO.ADMIN WHERE username='"+username+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_admin = data[0]
        return render(request,'dashboard_admin.html',{'general_data':general_data,'data_admin':data_admin})

    if role == 'pengguna_publik':
        select="SELECT * FROM SIRUCO.PENGGUNA_PUBLIK WHERE username='"+username+"';"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        data_pengguna_publik = data[0]
        return render(request,'dashboard_pengguna_publik.html',{'general_data':general_data,
        'data_pengguna_publik':data_pengguna_publik})

    elif role == 'dokter':
        select="SELECT * FROM SIRUCO.DOKTER WHERE username='"+username+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_dokter = data[0]
        return render(request,'dashboard_dokter.html',{'general_data':general_data,'data_dokter':data_dokter})

    elif role == 'admin_satgas':
        select="SELECT * FROM SIRUCO.ADMIN_SATGAS WHERE username='"+username+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_admin_satgas = data[0]
        return render(request,'dashboard_adminsatgas.html',{'general_data':general_data,'data_admin_satgas':data_admin_satgas})
